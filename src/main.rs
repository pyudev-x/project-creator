use std::fs;
use std::io;
use std::process;

const MAKEFILE_CONTENTS:&str = 
"CC=clang++
OUT=app

compile:
\t$(CC) main.cpp -o build/$(OUT)
run:
\t./build/$(OUT)";
const MAIN_CONTENTS:&str =
"#include <iostream>
int main() {
\tstd::cout << '0' << std::endl;
\treturn 0;
}";

fn main() {
    println!("Hello, world!");

    let name = read_line("Project name: ");    
    fs::create_dir(&name).unwrap();    

    let ptype = read_line("Project type(rust, cpp, elixir, empty(default)): ");
    
    match ptype.as_str() {
        "rust"=>{
            /*let mut cmd = */process::Command::new("cargo").arg("init").arg(name+"/").spawn().unwrap();

            // cmd.spawn().unwrap();
        }
        "cpp"=>{
            fs::File::create(format!("{}/Makefile", &name)).unwrap();
            fs::File::create(format!("{}/main.cpp", &name)).unwrap();
            fs::write(format!("{}/Makefile", &name), MAKEFILE_CONTENTS).unwrap();
            fs::write(format!("{}/main.cpp", &name), MAIN_CONTENTS).unwrap();

            fs::create_dir(format!("{}/build", &name)).unwrap();
        }
        "elixir"=>{
            fs::remove_dir(&name).unwrap();
            process::Command::new("mix").arg("new").arg(name+"/").spawn().unwrap();
        }
        _ => {
            fs::File::create(format!("{}/Makefile", &name)).unwrap();
        }
    }

}

fn read_line(input:&str) -> String {
    eprint!("{input}");

    let mut value:String = String::new();
    io::stdin().read_line(&mut value).unwrap_or(0);

    return value.trim().to_string();
}